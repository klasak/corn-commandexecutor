﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CommandExecutor
{
    class SoundRecorder
    {
        private String WAV_FILE = "command.wav";
        private String FLAC_FILE = "command.flac";

        private String programPath;

        public SoundRecorder()
        {
            programPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        }

        public void RecordSound()
        {
            StartRecording();
            ConvertFileToFlac();
            System.Threading.Thread.Sleep(1000);
        }

        private void StartRecording()
        {

            if (File.Exists(WAV_FILE))
            {
                File.Delete(WAV_FILE);
            }            

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = programPath+@"\components\requirements\sox\rec.exe";
            startInfo.Arguments = "-r 8000 -c 1 " + WAV_FILE;
            process.StartInfo = startInfo;
            process.Start();
            System.Threading.Thread.Sleep(4000);
            process.Kill();

        }

         private void ConvertFileToFlac()
        {
            if (File.Exists(FLAC_FILE))
            {
                File.Delete(FLAC_FILE);
            }

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = programPath + @"\components\requirements\sox\sox.exe"; 
            startInfo.Arguments = WAV_FILE + " -t flac -r 8k -b 16 -C 8 " + FLAC_FILE;
            process.StartInfo = startInfo;
            process.Start();

        }

    }
}
