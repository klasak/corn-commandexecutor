﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace CommandExecutor
{
    class SpeechRecognizer
    {
        private String FLAC_FILE = "command.flac";

        public SpeechRecognizer()
        {

        }

        public String GetInstruction()
        {
            String text = ProcessFlacFile(FLAC_FILE);
            return getMessage(text);
        }

        private String ProcessFlacFile(string path)
        {
            HttpWebRequest request =
                 (HttpWebRequest)HttpWebRequest.Create(
                 "https://www.google.com/speech-api/v2/recognize?lang=pl-PL&key=AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw");

            ServicePointManager.ServerCertificateValidationCallback +=
                         delegate { return true; };

            request.Timeout = 60000;
            request.Method = "POST";
            request.KeepAlive = true;
            request.ContentType = "audio/x-flac; rate=8000";
            request.UserAgent = "speech2text";

            FileInfo fInfo = new FileInfo(path);
            long numBytes = fInfo.Length;
            byte[] data;
            String result = null;

            using (FileStream fStream = new FileStream(
                                              path,
                                              FileMode.Open,
                                              FileAccess.Read))
            {
                data = new byte[fStream.Length];
                fStream.Read(data, 0, (int)fStream.Length);
                fStream.Close();
            }

            using (Stream wrStream = request.GetRequestStream())
                wrStream.Write(data, 0, data.Length);

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var resp = response.GetResponseStream();
                

                if (resp != null)
                {
                    StreamReader sr = new StreamReader(resp);
                    result = sr.ReadToEnd();
                    resp.Close();
                    resp.Dispose();
                }
            }
            catch (System.Exception ee)
            {
                Console.WriteLine(ee.Message);
            }
            return result;
        }

        private String getMessage(String text)
        {
            String[] splitted = Regex.Split(text, "\n");
            Console.WriteLine(text);
            int count = splitted.Count();
            Response[] responses = new Response[count];
            Response mainResponse = null;
            for (int i = 0; i < count; ++i)
            {
                responses[i] = JsonConvert.DeserializeObject<Response>(splitted[i]);
                if (responses[i] != null && responses[i].Result.Count() > 0)
                {
                    mainResponse = responses[i];
                    break;
                }
            }
            String message = null;

            if (mainResponse == null)
            {
                return "";
            }
            Result result = mainResponse.Result.First<Result>();  
            Alternative firstAlternative = result.Alternative.First<Alternative>();
            message = firstAlternative.Transcript;

            return message;
        }

    }
}
