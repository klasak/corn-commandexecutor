﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandExecutor
{
    class Alternative
    {
        private String transcript;
        private double confidence;

        public String Transcript
        {
            set { this.transcript = value; }
            get { return this.transcript; }
        }

        public double Confidence
        {
            set { this.confidence = value; }
            get { return this.confidence; }
        }
    }
}
