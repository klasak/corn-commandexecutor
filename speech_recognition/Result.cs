﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandExecutor
{
    class Result
    {
        private List<Alternative> alternative;
        private bool final;

        public List<Alternative> Alternative
        {
            set { this.alternative = value; }
            get { return this.alternative; }
        }

        public bool Final
        {
            set { this.final = value; }
            get { return this.final; }
        }
    }
}
