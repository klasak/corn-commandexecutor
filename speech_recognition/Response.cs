﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandExecutor
{
    class Response
    {
        private int result_index;
        private List<Result> result;

        public int Result_index
        {
            set { this.result_index = value; }
            get { return this.result_index; }
        }

        public List<Result> Result
        {
            set { this.result = value; }
            get { return this.result; }
        }
    }
}
