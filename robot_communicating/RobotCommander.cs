﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aldebaran.Proxies;

namespace CommandExecutor
{
    class RobotCommander
    {

        private String ip;
        private int port;

        public String Ip
        {
            set { this.ip = value; }
            get { return this.ip; }
        }

        public int Port
        {
            set { this.port = value; }
            get { return this.port; }
        }

        public bool Command(String message)
        {
            String command = message.ToLower();

            Console.WriteLine(command);

            if (command.StartsWith("say"))
            {
                String text = command.Replace("say ", "");
                CommandSay(text);
                return true;
            }
            else if (command.StartsWith("tell me"))
            {
                String text = command.Replace("tell me ", "");
                CommandSay(text);
                return true;
            }
            else if (command.StartsWith("tell"))
            {
                String text = command.Replace("tell ", "");
                CommandSay(text);
                return true;
            }
            else if (command.Contains("open") && command.Contains("left"))
            {
                CommandOpenLeftHand();
                return true;
            }
            else if (command.Contains("open") && command.Contains("right"))
            {
                CommandOpenRightHand();
                return true;
            }
            else if (command.Contains("close") && command.Contains("left"))
            {
                CommandCloseLeftHand();
                return true;
            }
            else if (command.Contains("close") && command.Contains("right"))
            {
                CommandCloseRightHand();
                return true;
            }
            return false;
        }

        private void CommandSay(String text)
        {
            try
            {
                TextToSpeechProxy tts = new TextToSpeechProxy(ip, port);
                tts.say(text);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void CommandOpenLeftHand()
        {
            try
            {
                MotionProxy mp = new MotionProxy(ip, port);
                mp.openHand("LHand");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void CommandOpenRightHand()
        {
            try
            {
                MotionProxy mp = new MotionProxy(ip, port);
                mp.openHand("RHand");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void CommandCloseLeftHand()
        {
            try
            {
                MotionProxy mp = new MotionProxy(ip, port);
                mp.closeHand("LHand");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void CommandCloseRightHand()
        {
            try
            {
                MotionProxy mp = new MotionProxy(ip, port);
                mp.closeHand("RHand");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


    }
}
