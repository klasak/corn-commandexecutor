﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace CommandExecutor
{
    public class CommandExecutor : CORN.Components.Component
    {
        private Grid grid;
        private Button button;
        private Label label;

        private SoundRecorder soundRecorder;
        private SpeechRecognizer recognizer;
        private Translator translator;
        private RobotCommander robotCommander;

       

        public CommandExecutor()
        {
            soundRecorder = new SoundRecorder();
            recognizer = new SpeechRecognizer();
            translator = new Translator();
            robotCommander = new RobotCommander();

            grid = new Grid();
            button = new Button();
            label = new Label();

            button.Content = "Podaj komendę";
            button.Click += new RoutedEventHandler(button_Click);

            label.Content = "";
            label.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

            grid.Children.Add(button);
            grid.Children.Add(label);

        }

        public UIElement GetContent()
        {
            return grid;
        }

        public String GetName()
        {
            return "Polish Commander";
        }

        public void Start(String ip, int port)
        {
            robotCommander.Ip = ip;
            robotCommander.Port = port;
        }

        public void Stop()
        {

        }

        void button_Click(object sender, RoutedEventArgs e)
        {
            StartRecording();
        }

        void StartRecording()
        {
            soundRecorder.RecordSound();
            String polishInstruction = recognizer.GetInstruction();
            String englishInstruction = translator.Translate(polishInstruction);
            if (robotCommander.Command(englishInstruction))
            {
                label.Content = "Robot wykonuje instrukcję";
            }
            else
            {
                label.Content = "Robot nie potrafi wykonać instrukcji";
            }
            
        }

    }
}
